#solve 2 dimentional linear regression with matrix inverse method

import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
sess = tf.Session()
x_vals = np.linspace(0, 10, 100)
y_vals = x_vals + np.random.normal(0, 1, 100)

#a matrix will be column of x data and many 1s. b is from y

x_vals_column = np.transpose(np.matrix(x_vals))
ones_column = np.transpose(np.matrix(np.repeat(1,100)))

A =  np.column_stack((x_vals_column, ones_column))
b = np.transpose(np.matrix(y_vals))

#now we will turn A and b matrix into tensors.

A_tensor =  tf.constant(A)
b_tensor = tf.constant(b)

# now, matrix is setup. we will solve Ax = b or, x= (A^T . A)^-1 . A^T. b equation

tA_A = tf.matmul(tf.transpose(A_tensor), A_tensor)
tA_A_inv = tf.matrix_inverse(tA_A)
product =  tf.matmul(tA_A_inv, tf.transpose(A_tensor))
solution = tf.matmul(product, b_tensor)
solution_eval = sess.run(solution)

#now we exreact the cofficient from the solution and plot it
slope = solution_eval[0] [0]
y_intercept = solution_eval[1][0]
print('slopr= ' + str(slope))
print('y intercept= ' + str(y_intercept))

#Get best fit line
best_fit=[]
for i in x_vals:
    best_fit.append(slope*i+y_intercept)
    
#now, the plot

plt.plot(x_vals, y_vals, 'o' , label = 'data')
plt.plot(x_vals, best_fit, 'r-', label = 'best fit line', linewidth = 5)
plt.legend(loc='upper left')
plt.show()
    
