import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt

np.random.seed(101)
tf.set_random_seed(101)

#generating random linear data
x = np.linspace(0, 50, 50)
y = np.linspace(0, 50, 50)

#adding noise to the data
x += np.random.uniform(-4, 4, 50)
y += np.random.uniform(-4, 4, 50)


#num of data points

n = len(x)

#plot of training data
plt.scatter(x, y)
plt.show()

#now defining placeholder upper X and Y

X = tf.placeholder("float")
Y = tf.placeholder("float")

#now, 2 trainable tensorflow variable, weight and bias

w = tf.Variable(np.random.rand(), name = "w")
b = tf.Variable(np.random.rand(), name = "b")

#now hyperparameter of models. learning rate and Epochs

learning_rate = 0.01
training_epochs = 1000

#now we will build hypothesis, cost function and optimizer

#hypothesis
y_pred = tf.add(tf.multiply(X, w), b)

#mean squre error cost
cost = tf.reduce_sum(tf.pow(y_pred - Y, 2)) / (2 *n)

#GDO
optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(cost)

init = tf.global_variables_initializer()

#starting with tensorflow session

with tf.Session() as sess:
    sess.run(init)  #initialize variable
    for epoch in range(training_epochs):      #training through all epoch
        for(_x, _y) in zip(x,y):
            sess.run(optimizer, feed_dict = {X:_x, Y:_y})    #feeding each data point into the optimizer using feed Dictionary
        if(epoch+1) % 50 ==0:
            c = sess.run(cost, feed_dict ={X: x, Y: y})
            print("epoch", (epoch +1), " : cost = ", c, "W=" , sess.run(w), "b=", sess.run(b))
            
    training_cost = sess.run(cost, feed_dict= {X: x, Y: y})     #sorting necessary values using outside session
    weight = sess.run(w)
    bias = sess.run(b)
    
    
#calculating the predictions
    
predictions = weight*x + bias
print("training cost=" , training_cost, "Weight = ", weight, "bias=", bias, '\n')


#ploting the resealt

plt.plot(x, y, 'ro', label='original data')
plt.plot(x, predictions, label = 'fitted data')
plt.title('Linear regression data')
plt.legend()
plt.show()





















