import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plot

num_points = 1000
vectors_set= []

for i in range(num_points):
    W = 0.1
    b = 0.4
    x1 = np.random.normal(0.0, 1.0)
    nd = np.random.normal(0.0, 0.1)
    y1 = W *x1 + b
    y1 = y1 + nd
    
    vectors_set.append([x1, y1])
    
    x_data = [v[0] for v in vectors_set]
    y_data = [v[1] for v in vectors_set]
    
plot.plot(x_data, y_data, 'ro', label='original data')
plot.xlim(-2,2)
plot.ylim(0.1, 0.6)
plot.legend()
plot.show()


with tf.name_scope("LinReg") as slope:
    
    W = tf.Variable(tf.zeros([1]))
    b = tf.Variable(tf.zeros([1]))
    y = W* x_data + b

with tf.name_scope("LosFn") as slope:
    loss = tf.reduce_mean(tf.square(y - y_data))

optimizer = tf.train.GradientDescentOptimizer(0.6)
train = optimizer.minimize(loss)

init = tf.global_variables_initializer()

sess = tf.Session()
sess.run(init)

for i in range(60):
    sess.run(train)
    if i%5 == 0 :
     print(i, "Weights= ", sess.run(W), "Bias= ", sess.run(b),"Loss= ", sess.run(loss))
    
    
    
plot.plot(x_data, y_data, 'ro', label= 'original')
plot.plot(x_data, sess.run(W)*x_data+sess.run(b))
plot.xlabel('X')
plot.xlim(-2,2)
plot.ylim(0.1, 0.6)
plot.ylabel('Y')
plot.legend()
plot.show()


loss_summary = tf.summary.scalar("loss", loss)
w1 = tf.summary.histogram("W", W)
b1 = tf.summary.histogram("b", b)
merged_op = tf.summary.merge_all()
writer_tensorboard= tf.summary.FileWriter('/tmp/tens/', tf.get_default_graph())





